﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameCountDown : MonoBehaviour {

	public float gameTime = 60f;
	public Text countDownDisplay;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		gameTime -= Time.deltaTime;
		countDownDisplay.text = gameTime.ToString ("0");
	}
	
}
