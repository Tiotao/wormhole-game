﻿using UnityEngine;
using System.Collections;

public class CrateEffect : MonoBehaviour {

	public int cashAmount = 20;
	GameObject player;
	PlayerAssets PlayerAssets;
	bool carteCollected = false;
	
	// Use this for initialization
	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player");
		PlayerAssets = player.GetComponent <PlayerAssets> ();
	}
	
	void OnTriggerEnter (Collider other)
	{
		if(other.gameObject == player && !carteCollected)
		{
			PlayerAssets.AddCash(cashAmount);
			carteCollected = false;
			Destroy (gameObject, 0f);
		}
	}
	
	
	void OnTriggerExit (Collider other)
	{
		
	}
}
