﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Maze : MonoBehaviour {

	public IntVector2 size;
	public MazeCell cellPrefab;
	public float generationStepDelay;
	public MazePassage passagePrefab;
	public MazeWall wallPrefab;

	private MazeCell[,] cells;

	private GameObject player;
	private CharacterMotor playerCharacterMotor;

	public int zombieAmount = 5;
	public int potionAmount = 5;
	public int crateAmount = 5;
	public int golemAmount = 5;
	public GameObject golemInstance;
	public GameObject zombieInstance;
	public GameObject potionInstance;
	public GameObject crateInstance;

	public IntVector2 RandomCoordinates {
		get {
			return new IntVector2(Random.Range(0, size.x), Random.Range(0, size.z));
		}
	}

	public bool ContainsCoordinates (IntVector2 coordinate) {
		return coordinate.x >= 0 && coordinate.x < size.x && coordinate.z >= 0 && coordinate.z < size.z;
	}

	public MazeCell GetCell (IntVector2 coordinates) {
		return cells[coordinates.x, coordinates.z];
	}

	public IEnumerator Generate () {

		GameObject leftWall = GameObject.Find ("Long Wall Left");
		GameObject rightWall = GameObject.Find ("Long Wall Right");
		float leftNewZ = 100f + 2 * size.z;
		float NewX = -2 * (size.x+1) + 0.2f;
		float rightNewZ = -100f - 2 * size.z;
		leftWall.transform.position = new Vector3 (NewX, leftWall.transform.position.y, leftNewZ);
		rightWall.transform.position = new Vector3 (NewX, rightWall.transform.position.y, rightNewZ);
		player = GameObject.FindGameObjectWithTag ("Player");
		playerCharacterMotor = player.GetComponent <CharacterMotor> ();
		WaitForSeconds delay = new WaitForSeconds(generationStepDelay);
		cells = new MazeCell[size.x, size.z];
		List<MazeCell> activeCells = new List<MazeCell>();
		DoFirstGenerationStep(activeCells);
		while (activeCells.Count > 0) {
			yield return delay;
			DoNextGenerationStep(activeCells);
		}
		playerCharacterMotor.movement.gravity = 9.81f;
		for (int i = 0; i < zombieAmount; i++) {
			float randomX = Random.Range (-2 * size.x, 2 * size.x);
			float randomZ = Random.Range (-2 * size.z, 2 * size.z);
			Instantiate(zombieInstance, new Vector3(randomX, 0, randomZ), Quaternion.identity);
			
		}

		for (int i = 0; i < golemAmount; i++) {
			float randomX = Random.Range (-2 * size.x, 2 * size.x);
			float randomZ = Random.Range (-2 * size.z, 2 * size.z);
			Instantiate(golemInstance, new Vector3(randomX, 0, randomZ), Quaternion.identity);
			
		}

		GameObject exitCell = GameObject.Find ("Maze Cell " + (size.x - 1) + ", " + (size.z - 2));
		GameObject entryCell = GameObject.Find ("Maze Cell 0, 1");

		Destroy (exitCell, 0f);
		Destroy (entryCell, 0f);
		for (int i = 0; i < potionAmount; i++) {
			float randomX = Random.Range (-20f, 20f);
			float randomZ = Random.Range (-20f, 20f);
			Instantiate(potionInstance, new Vector3(randomX, 0.2f, randomZ), Quaternion.identity);
			
		}

		for (int i = 0; i < crateAmount; i++) {
			float randomX = Random.Range (-20f, 20f);
			float randomZ = Random.Range (-20f, 20f);
			Instantiate(crateInstance, new Vector3(randomX, 0.4f, randomZ), Quaternion.identity);
			
		}
	}

	private void DoFirstGenerationStep (List<MazeCell> activeCells) {
		activeCells.Add(CreateCell(RandomCoordinates));
	}

	private void DoNextGenerationStep (List<MazeCell> activeCells) {
		int currentIndex = activeCells.Count - 1;
		MazeCell currentCell = activeCells[currentIndex];
		if (currentCell.IsFullyInitialized) {
			activeCells.RemoveAt(currentIndex);
			return;
		}
		MazeDirection direction = currentCell.RandomUninitializedDirection;
		IntVector2 coordinates = currentCell.coordinates + direction.ToIntVector2();
		if (ContainsCoordinates(coordinates)) {
			MazeCell neighbor = GetCell(coordinates);
			if (neighbor == null) {
				neighbor = CreateCell(coordinates);
				CreatePassage(currentCell, neighbor, direction);
				activeCells.Add(neighbor);
			}
			else {
				CreateWall(currentCell, neighbor, direction);
			}
		}
		else {
			CreateWall(currentCell, null, direction);
		}
	}

	private MazeCell CreateCell (IntVector2 coordinates) {
		MazeCell newCell = Instantiate(cellPrefab) as MazeCell;
		cells[coordinates.x, coordinates.z] = newCell;
		newCell.coordinates = coordinates;
		newCell.name = "Maze Cell " + coordinates.x + ", " + coordinates.z;
		newCell.transform.parent = transform;
		newCell.transform.localPosition = new Vector3(coordinates.x - size.x * 0.5f + 0.5f, 0f, coordinates.z - size.z * 0.5f + 0.5f);
		return newCell;
	}

	private void CreatePassage (MazeCell cell, MazeCell otherCell, MazeDirection direction) {
		MazePassage passage = Instantiate(passagePrefab) as MazePassage;
		passage.Initialize(cell, otherCell, direction);
		passage = Instantiate(passagePrefab) as MazePassage;
		passage.Initialize(otherCell, cell, direction.GetOpposite());
	}

	private void CreateWall (MazeCell cell, MazeCell otherCell, MazeDirection direction) {
		MazeWall wall = Instantiate(wallPrefab) as MazeWall;
		wall.Initialize(cell, otherCell, direction);
		if (otherCell != null) {
			wall = Instantiate(wallPrefab) as MazeWall;
			wall.Initialize(otherCell, cell, direction.GetOpposite());
		}
	}
}