﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerAssets : MonoBehaviour {

	public int startingCash = 0;
	public int currentCash;
	public Text cashAmountDisplay;

	// Use this for initialization
	void Awake () {
		currentCash = startingCash;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddCash (int amount)
	{
		
		currentCash += amount;

		cashAmountDisplay.text = currentCash.ToString ();
		
		//playerAudio.Play ();
		
		
	}
}
