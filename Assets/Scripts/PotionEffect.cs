﻿using UnityEngine;
using System.Collections;

public class PotionEffect : MonoBehaviour {

	public int healthIncrease = 20;
	GameObject player;
	PlayerHealth playerHealth;
	bool potionUsed = false;

	// Use this for initialization
	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
	}
	
	void OnTriggerEnter (Collider other)
	{
		if(other.gameObject == player && !potionUsed)
		{
			playerHealth.UsePotion(healthIncrease);
			potionUsed = false;
			Destroy (gameObject, 0f);
		}
	}
	
	
	void OnTriggerExit (Collider other)
	{

	}
}
