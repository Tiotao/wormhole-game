﻿using UnityEngine;
using System.Collections;

public class ZombieAttack : MonoBehaviour {

	public float timeBetweenAttacks = 0.5f;
	public int attackDamage = 10;
	public AudioClip attackClip;
	
	Animator anim;
	GameObject player;
	PlayerHealth playerHealth;
	AudioSource zombieAudio;
	ZombieHealth zombieHealth;
	bool playerInRange;
	public float timer;
	
	
	void Awake ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
		zombieAudio = GetComponent <AudioSource> ();
		zombieHealth = GetComponent<ZombieHealth>();
		anim = GetComponent <Animator> ();
	}
	
	
	void OnTriggerEnter (Collider other)
	{
		if(other.gameObject == player)
		{
			playerInRange = true;

		}
	}
	
	
	void OnTriggerExit (Collider other)
	{
		if(other.gameObject == player)
		{
			playerInRange = false;
		}
	}
	
	
	void Update ()
	{
		timer += Time.deltaTime;
		Debug.Log (timer >= timeBetweenAttacks);

		if(timer >= timeBetweenAttacks && playerInRange && zombieHealth.currentHealth > 0)
		{
			if(anim){
				anim.SetTrigger ("Attack");
			} else {
				animation.Play ("punch",PlayMode.StopSameLayer);
			}
			Attack ();
		}
		/*
		
		if(playerHealth.currentHealth <= 0)
		{
			anim.SetTrigger ("PlayerDead");
		}*/
	}
	
	
	void Attack ()
	{
		timer = 0f;
		zombieAudio.clip = attackClip;
		zombieAudio.Play ();

		if(playerHealth.currentHealth > 0)
		{
			playerHealth.TakeDamage (attackDamage);

		}

	}
}
