﻿using UnityEngine;
using System.Collections;

public class ZombieHealth : MonoBehaviour {

	public int startingHealth = 100;
	public int currentHealth;
	public float sinkSpeed = 2.5f;
	public int scoreValue = 10;
	public AudioClip deathClip;
	
	
	Animator anim;
	AudioSource zombieAudio;
	//ParticleSystem hitParticles;
	CapsuleCollider capsuleCollider;
	bool isDead;
	bool isSinking;
	
	
	void Awake ()
	{
		anim = GetComponent <Animator> ();
		zombieAudio = GetComponent <AudioSource> ();
		//hitParticles = GetComponentInChildren <ParticleSystem> ();
		capsuleCollider = GetComponent <CapsuleCollider> ();
		
		currentHealth = startingHealth;
	}
	
	
	void Update ()
	{
		if(isSinking)
		{
			transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
		}
	}
	
	
	public void TakeDamage (int amount)
	{
		if(isDead)
			return;
		
		//enemyAudio.Play ();
		
		currentHealth -= amount;
		
		//hitParticles.transform.position = hitPoint;
		//hitParticles.Play();
		
		if(currentHealth <= 0)
		{
			Death ();
		}
	}
	
	
	void Death ()
	{
		isDead = true;
		
		capsuleCollider.isTrigger = true;
		if (anim) {
			anim.SetTrigger ("Dead");
		} else {
			animation.Play("death");
		}

		zombieAudio.clip = deathClip;
		zombieAudio.Play ();
		StartCoroutine(StartSinking ());

	}
	
	
	public IEnumerator StartSinking ()
	{
		yield return new WaitForSeconds(3f);
		//GetComponent <NavMeshAgent> ().enabled = false;
		GetComponent <Rigidbody> ().isKinematic = true;
		isSinking = true;
		//ScoreManager.score += scoreValue;
		Destroy (gameObject, 2f);
	}
}


